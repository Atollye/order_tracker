# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/anna/Dropbox/order_tracker/ui_mainForm.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ot_window(object):
    def setupUi(self, ot_window):
        ot_window.setObjectName("ot_window")
        ot_window.resize(1161, 812)
        self.table_of_orders = QtWidgets.QTableWidget(ot_window)
        self.table_of_orders.setGeometry(QtCore.QRect(20, 20, 1051, 581))
        self.table_of_orders.setRowCount(20)
        self.table_of_orders.setColumnCount(10)
        self.table_of_orders.setObjectName("table_of_orders")
        self.newOrder_btn = QtWidgets.QPushButton(ot_window)
        self.newOrder_btn.setGeometry(QtCore.QRect(130, 650, 401, 41))
        self.newOrder_btn.setObjectName("newOrder_btn")
        self.chosenOrder_lbl = QtWidgets.QLabel(ot_window)
        self.chosenOrder_lbl.setGeometry(QtCore.QRect(40, 610, 131, 31))
        self.chosenOrder_lbl.setObjectName("chosenOrder_lbl")
        self.chosenOrder_lnEd = QtWidgets.QLineEdit(ot_window)
        self.chosenOrder_lnEd.setGeometry(QtCore.QRect(170, 610, 113, 27))
        self.chosenOrder_lnEd.setObjectName("chosenOrder_lnEd")
        self.delOrder_btn = QtWidgets.QPushButton(ot_window)
        self.delOrder_btn.setGeometry(QtCore.QRect(20, 720, 111, 41))
        self.delOrder_btn.setObjectName("delOrder_btn")
        self.backward_btn = QtWidgets.QPushButton(ot_window)
        self.backward_btn.setGeometry(QtCore.QRect(160, 720, 211, 41))
        self.backward_btn.setObjectName("backward_btn")
        self.handOutTheOrder_btn = QtWidgets.QPushButton(ot_window)
        self.handOutTheOrder_btn.setGeometry(QtCore.QRect(650, 720, 321, 41))
        self.handOutTheOrder_btn.setObjectName("handOutTheOrder_btn")
        self.showArchive_btn = QtWidgets.QPushButton(ot_window)
        self.showArchive_btn.setGeometry(QtCore.QRect(1080, 20, 71, 91))
        self.showArchive_btn.setObjectName("showArchive_btn")
        self.forward_btn = QtWidgets.QPushButton(ot_window)
        self.forward_btn.setGeometry(QtCore.QRect(390, 720, 211, 41))
        self.forward_btn.setObjectName("forward_btn")

        self.retranslateUi(ot_window)
        QtCore.QMetaObject.connectSlotsByName(ot_window)

    def retranslateUi(self, ot_window):
        _translate = QtCore.QCoreApplication.translate
        ot_window.setWindowTitle(_translate("ot_window", "Form"))
        self.newOrder_btn.setText(_translate("ot_window", "Новый заказ"))
        self.chosenOrder_lbl.setText(_translate("ot_window", "Выбран заказ №"))
        self.delOrder_btn.setText(_translate("ot_window", "Удалить заказ"))
        self.backward_btn.setText(_translate("ot_window", "Назад"))
        self.handOutTheOrder_btn.setText(_translate("ot_window", "Выдать заказ клиенту"))
        self.showArchive_btn.setText(_translate("ot_window", "Архив"))
        self.forward_btn.setText(_translate("ot_window", "Вперед"))

