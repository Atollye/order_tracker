#!/usr/bin/python3
# -*- coding: utf-8 -*-

# /home/atollye/Dropbox/order_tracker/main.py

#импортируем системные модули
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QTableWidgetItem
import sqlite3

#импортируем графический интерфейс основного окна
import mainForm_ui


class MainWindow(QMainWindow):   #класс для интерфейса главного окна 
    def __init__(self):
        super().__init__()
        self.ui = mainForm_ui.Ui_ot_window()
        self.ui.setupUi(self)
        self.show()
        self.upload_data()
        #добавляем обработчики сигналов от виджетов
        self.ui.table_of_orders.verticalHeader().sectionClicked.connect(self.chose_the_order)
        self.ui.delOrder_btn.clicked.connect(self.cancel_the_order)
        self.ui.handOutTheOrder_btn.clicked.connect(self.give_away)
        self.ui.forward_btn.clicked.connect(self.move_forward)
        self.ui.backward_btn.clicked.connect(self.move_backward)

    def upload_data(self):
        import sqlite3
        path = r'/home/atollye/Dropbox/otm/stationery_shop.sqlt3'
        con = sqlite3.connect(path)
        cur = con.cursor()
        cur.execute('SELECT order_id,name,phone,status,art_1,quant_1,descr_1,art_2,quant_2,descr_2 FROM orders')
        lst_from_db = cur.fetchall()
        #print(lst_from_db)
        con.close()
        for row_idx,row in enumerate(lst_from_db):
            lst = list(row)
            for column_idx, cell in enumerate(lst):
                cell_str = str(cell)
                item = QTableWidgetItem()
                item.setText(cell_str)
                self.ui.table_of_orders.setItem(row_idx,column_idx,item)

    def chose_the_order(self):
        cur_row=window.ui.table_of_orders.currentRow()
        itm_one=window.ui.table_of_orders.item(cur_row,0)
        if cur_row!=0:
            if itm_one:
                content = itm_one.text()
                window.ui.chosenOrder_lnEd.setText(str(content))

    def cancel_the_order(self):
        #определить, какой заказ выбран
        cur_row=self.ui.table_of_orders.currentRow()
        cur_row +=1
        #print(cur_row)/home/atollye/Dropbox/order_tracker/main.py
        path = r'/home/atollye/Dropbox/order_tracker/stationery_shop.sqlt3'
        con = sqlite3.connect(path)
        cur = con.cursor()
        sqlite3.paramstyle = 'pyformat'
        cur.execute("UPDATE orders SET status = 'cancelled' WHERE counter = %d" % (cur_row))
        con.commit()
        cur.close()
        con.close()
        #перезагрузить таблицу
        self.upload_data()
    
    def wrapper(self):
        f = self.give_away()
        if f:
            print()
    
    def give_away(self):
        #(1)получить номер и статус выбранного заказа
        lst = self.ui.table_of_orders.selectedItems()
        if len(lst)==0:
            print("No order selected")
            return 1
        elif len(lst)==10:
            cur_row=window.ui.table_of_orders.currentRow()
            order=window.ui.table_of_orders.item(cur_row,0).text()
            status=window.ui.table_of_orders.item(cur_row,3).text()
        else:
            print("Order is selected incorrectly")

        #(2)проверить, что у заказа статус "in_stock"
        if status != 'in_stock':
            return 'Неее'
        #(3)вывести окно "Выдать заказ XXX? -- пока пропускаем
        
        #(4)записать в базу данных статус заказа order_id "handed"
        path = r'/home/atollye/Dropbox/order_tracker/stationery_shop.sqlt3'
        con = sqlite3.connect(path)
        cur = con.cursor()
        cur.execute("UPDATE orders SET status = 'handed' WHERE order_id = ?", (order,))
        con.commit()
        cur.close()
        con.close()
        
        #(5)обновить таблицу с заказами
        self.upload_data()
        

    def move_forward(self):
        #получить номер заказа и статус
        cur_row=window.ui.table_of_orders.currentRow()
        #itm_one=window.ui.table_of_orders.item(cur_row,0)
        itm_one=window.ui.table_of_orders.item(cur_row,0)
        if not itm_one:
            return "mistake"
        order = itm_one.text()
        print(order)
        itm_two=window.ui.table_of_orders.item(cur_row,3)
        status = itm_two.text()
        #действие в зависимости от значения
        if status =='new':
            status = 'in_delievery'
        elif status == 'in_delievery':
            status = 'in_stock'
        elif status == 'in_stock':
            status = 'in_stock'
        
        
        #записать новое значение
        path = r'/home/atollye/Dropbox/order_tracker/stationery_shop.sqlt3'
        con = sqlite3.connect(path)
        cur = con.cursor()
        cur.execute("UPDATE orders SET status = ? WHERE order_id = ?", (status,order,))
        con.commit()
        cur.close()
        con.close()
        #обновить функцию из базы
        self.upload_data()

    def move_backward(self):
        print("Двинули назад")




if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.upload_data()
    #дописать строчку с quit, сохраняющую все измения в базе


    sys.exit(app.exec_())
