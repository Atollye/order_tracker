# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/atollye/Dropbox/otm/inputForm_ui.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_new_order_form(object):
    def setupUi(self, new_order_form):
        new_order_form.setObjectName("new_order_form")
        new_order_form.resize(583, 530)
        self.order_num_lbl = QtWidgets.QLabel(new_order_form)
        self.order_num_lbl.setGeometry(QtCore.QRect(40, 30, 141, 21))
        self.order_num_lbl.setObjectName("order_num_lbl")
        self.order_num_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.order_num_lnEd.setGeometry(QtCore.QRect(160, 20, 113, 27))
        self.order_num_lnEd.setObjectName("order_num_lnEd")
        self.name_lbl = QtWidgets.QLabel(new_order_form)
        self.name_lbl.setGeometry(QtCore.QRect(40, 70, 141, 21))
        self.name_lbl.setObjectName("name_lbl")
        self.phone_lbl = QtWidgets.QLabel(new_order_form)
        self.phone_lbl.setGeometry(QtCore.QRect(40, 110, 141, 21))
        self.phone_lbl.setObjectName("phone_lbl")
        self.article_lbl = QtWidgets.QLabel(new_order_form)
        self.article_lbl.setGeometry(QtCore.QRect(20, 180, 101, 21))
        self.article_lbl.setObjectName("article_lbl")
        self.descr_lbl = QtWidgets.QLabel(new_order_form)
        self.descr_lbl.setGeometry(QtCore.QRect(170, 190, 231, 16))
        self.descr_lbl.setObjectName("descr_lbl")
        self.quntity_lbl = QtWidgets.QLabel(new_order_form)
        self.quntity_lbl.setGeometry(QtCore.QRect(460, 180, 101, 21))
        self.quntity_lbl.setObjectName("quntity_lbl")
        self.art1_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.art1_lnEd.setGeometry(QtCore.QRect(10, 210, 113, 27))
        self.art1_lnEd.setObjectName("art1_lnEd")
        self.art2_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.art2_lnEd.setGeometry(QtCore.QRect(10, 260, 113, 27))
        self.art2_lnEd.setObjectName("art2_lnEd")
        self.art3_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.art3_lnEd.setGeometry(QtCore.QRect(10, 320, 113, 27))
        self.art3_lnEd.setObjectName("art3_lnEd")
        self.art4_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.art4_lnEd.setGeometry(QtCore.QRect(10, 380, 113, 27))
        self.art4_lnEd.setObjectName("art4_lnEd")
        self.descr1_txtEd = QtWidgets.QTextEdit(new_order_form)
        self.descr1_txtEd.setGeometry(QtCore.QRect(160, 210, 261, 41))
        self.descr1_txtEd.setObjectName("descr1_txtEd")
        self.descr2_txtEd = QtWidgets.QTextEdit(new_order_form)
        self.descr2_txtEd.setGeometry(QtCore.QRect(160, 260, 261, 41))
        self.descr2_txtEd.setObjectName("descr2_txtEd")
        self.descr3_txtEd = QtWidgets.QTextEdit(new_order_form)
        self.descr3_txtEd.setGeometry(QtCore.QRect(160, 320, 261, 41))
        self.descr3_txtEd.setObjectName("descr3_txtEd")
        self.descr4_txtEd = QtWidgets.QTextEdit(new_order_form)
        self.descr4_txtEd.setGeometry(QtCore.QRect(160, 380, 261, 41))
        self.descr4_txtEd.setObjectName("descr4_txtEd")
        self.quant1_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.quant1_lnEd.setGeometry(QtCore.QRect(460, 210, 81, 31))
        self.quant1_lnEd.setObjectName("quant1_lnEd")
        self.quant2_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.quant2_lnEd.setGeometry(QtCore.QRect(460, 260, 81, 31))
        self.quant2_lnEd.setObjectName("quant2_lnEd")
        self.quant3_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.quant3_lnEd.setGeometry(QtCore.QRect(460, 320, 81, 31))
        self.quant3_lnEd.setObjectName("quant3_lnEd")
        self.quant4_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.quant4_lnEd.setGeometry(QtCore.QRect(460, 380, 81, 31))
        self.quant4_lnEd.setObjectName("quant4_lnEd")
        self.create_order_btn = QtWidgets.QPushButton(new_order_form)
        self.create_order_btn.setGeometry(QtCore.QRect(290, 450, 261, 51))
        self.create_order_btn.setObjectName("create_order_btn")
        self.name_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.name_lnEd.setGeometry(QtCore.QRect(160, 60, 341, 31))
        self.name_lnEd.setObjectName("name_lnEd")
        self.phone_lnEd = QtWidgets.QLineEdit(new_order_form)
        self.phone_lnEd.setGeometry(QtCore.QRect(160, 100, 381, 31))
        self.phone_lnEd.setObjectName("phone_lnEd")

        self.retranslateUi(new_order_form)
        QtCore.QMetaObject.connectSlotsByName(new_order_form)

    def retranslateUi(self, new_order_form):
        _translate = QtCore.QCoreApplication.translate
        new_order_form.setWindowTitle(_translate("new_order_form", "Form"))
        self.order_num_lbl.setText(_translate("new_order_form", "Номер заказа"))
        self.name_lbl.setText(_translate("new_order_form", "Имя"))
        self.phone_lbl.setText(_translate("new_order_form", "Телефон"))
        self.article_lbl.setText(_translate("new_order_form", "артикул"))
        self.descr_lbl.setText(_translate("new_order_form", "текстовое описание"))
        self.quntity_lbl.setText(_translate("new_order_form", "количество"))
        self.create_order_btn.setText(_translate("new_order_form", "Сохранить заказ"))

