#!/usr/bin/python3
# -*- coding: utf-8 -*-

# /home/atollye/Dropbox/otm/add_new_order.py

#импортируем модули из стандартных графических библиотек
import sys
from PyQt5.QtWidgets import QWidget, QApplication

#импортируем класс графического интерфейса
import inputForm_ui 

#импортируем библиотеку для работы с регулярными выражениями
import re
import sqlite3


class Add_new_order():
    
    
     
    def __init__(self): 
        self.widg = QWidget()
        self.ui = inputForm_ui.Ui_new_order_form()
        self.ui.setupUi(self.widg)
        self.widg.show()            
        self.ui.create_order_btn.clicked.connect(self.insert_into_database)
        #проверка доступа к импортированным виджетам
        #self.ui.create_order_btn.setText("Тееееекст")

    def generate_identifier(self):
        return '08-10-17-4'
                """" заполняет поле 'дата заказа' """
        import datetime
        now = datetime.datetime.now()
        time_of_order =  now.strftime('%Y-%m-%d %H:%M')
        #>>> time_of_order
        #'2017-10-16 10:27'
        return time_of_order


    def check_data_format(self):
        def check_name(ipt):
            name_regex = re.compile(r'^[а-яА-ЯёЁ -]+$')
            mo = name_regex.fullmatch(ipt)
            if mo:
                #print("correct name") #нужна подстановка из следующей строки
                print(ipt)
                return ipt
            else:
                #print("Incorrect name")
                m = "Incorrect name"
        
        def check_phone(ipt):
            #print(ipt)
            phone_regex = re.compile(r'^(\+7|8)?(\(|-)*\d\d\d(-|\))*\d\d\d-\d\d\-\d\d$')
            mo = phone_regex.fullmatch(ipt)
            if mo:
                print(ipt)
                return ipt
            else:
                #print("Incorrect phone number")
                m = "Incorrect phone number"
        
        def check_article(ipt):
            article_regex = re.compile(r'^(\d){4,9}$')
            mo = article_regex.fullmatch(ipt)
            if mo:
                #print("correct name") #нужна подстановка из следующей строки
                print(ipt)
                return ipt
            else:
                #print("Incorrect article")
                 m = "Incorrect article"
        
        def check_description(ipt):
            description_regex = re.compile(r'^[а-яА-ЯёЁa-zA-Z0-9 ,()]{4,200}$')
            mo = description_regex.fullmatch(ipt)
            if mo:
                #print("correct name") #нужна подстановка из следующей строки
                print(ipt)
                return ipt
            else:
                #print("Incorrect description")
                m = "Incorrect description"
        
        def check_quantity(ipt):
            if ipt.isdigit() and ipt[0]!='0':
                if int(ipt)<500:
                    print(ipt)
                    return ipt
                else:
                    print("Too big quantity")
            else:
                #print("Incorrect quantity")
                m = "Incorrect quantity"
        # HERE 
        # while-цикл проверки на правильность заполнения формы
        
        #непонятно, как обрабатывать в функциях None объекты, если поля 
        #не заполнены
        
        self.fill_the_lst()
        #вопрос иерархии вложенности -- внутри какой структуры
        # лучше реализовать fill_the_lst
        
    def fill_the_lst():
        #№окончательный проход, когда все формы заполнены верно
        new_order = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        new_order[0] = self.generate_counter()
        new_order[1] = self.generate_id()
        new_order[2] = check_name(self.ui.name_lnEd.text())
        new_order[3] = check_phone(self.ui.phone_lnEd.text())
        new_order[4] = check_article(self.ui.art1_lnEd.text())
        new_order[5] = check_description(self.ui.descr1_txtEd.toPlainText())
        new_order[6] = check_quantity(self.ui.quant1_lnEd.text())
        new_order[7] = check_article(self.ui.art2_lnEd.text())
        new_order[7] = check_description(self.ui.descr2_txtEd.toPlainText())
        new_order[8] = check_quantity(self.ui.quant2_lnEd.text())
        new_order[9] = check_article(self.ui.art3_lnEd.text())
        new_order[10] = check_description(self.ui.descr3_txtEd.toPlainText())
        new_order[11] = check_quantity(self.ui.quant3_lnEd.text())
        new_order[12] = check_article(self.ui.art4_lnEd.text())
        new_order[13] = check_description(self.ui.descr4_txtEd.toPlainText())
        new_order[14] = check_quantity(self.ui.quant4_lnEd.text())
        new_order[15] = "comment"
        print(new_order)

    def insert_into_database(self):            
        path = r'/home/atollye/Dropbox/otm/shop.sqlt3'
        con = sqlite3.connect(path)
        cur = con.cursor()
        new_order = ['08-10-17-1', 'Анна', '8-916-926-18-09', '723041', 'Моноблок HP ProOne 440 G3 (1KN95EA)', '1', '', '', '', '', '', '', '', '', 'comment', 'called']
        cur.execute("INSERT INTO orders VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", new_order) 
        #в поле counter должен быть тип INTEGER и значение "первичный ключ"
        con.commit()
        con.close()
       


        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    add_no = Add_new_order()
    
    sys.exit(app.exec_())

#поменять поле с номером заказа c lineEdit на label
#куда впихнуть всплывающие сообщения для второго, третьего 
#и четвертого товара, которых может не быть???
